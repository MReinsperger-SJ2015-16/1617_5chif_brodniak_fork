package spengergasse.broellrei.thymeleaf;

import org.junit.Before;
import org.junit.Test;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.testing.templateengine.engine.TestExecutor;
import spengergasse.broellrei.dialect.MenuDialect;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class DropdownThymeleafTest {
    final TestExecutor executor = new TestExecutor();


    @Before
    public void setupTests() {
        final List<IDialect> dialects = new ArrayList<>();
        dialects.add(new MenuDialect());

        executor.setDialects(dialects);
    }

    @Test
    public void testDropdownDialectDemo() {
        executor.execute("file:src/test/resources/thymeleaf/dropdown_demo.thtest");
        assertTrue(executor.isAllOK());
    }

    @Test
    public void testDropdownDialectDeep() {
        executor.execute("file:src/test/resources/thymeleaf/dropdown_deep.thtest");
        assertTrue(executor.isAllOK());
    }

    @Test
    public void testDropdownDialectHuge() {
        executor.execute("file:src/test/resources/thymeleaf/dropdown_huge.thtest");
        assertTrue(executor.isAllOK());
    }
}
