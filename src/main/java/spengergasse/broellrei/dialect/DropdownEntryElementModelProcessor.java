package spengergasse.broellrei.dialect;

import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.AbstractElementModelProcessor;
import org.thymeleaf.processor.element.IElementModelStructureHandler;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.templatemode.TemplateMode;

public class DropdownEntryElementModelProcessor extends AbstractElementModelProcessor {

    public DropdownEntryElementModelProcessor(final String dialectPrefix) {

        super(
                TemplateMode.HTML,  // This processor will apply only to HTML mode
                dialectPrefix,      // Prefix to be applied to name for matching
                "dropentry",        // No tag name: match any tag name
                false,              // No prefix to be applied to tag name
                null,               // Name of the attribute that will be matched
                false,              // Apply dialect prefix to attribute name
                1000000);           // Precedence (inside dialect's precedence)
    }

    protected void doProcess(ITemplateContext context,
                             IModel model,
                             IElementModelStructureHandler structureHandler) {

        IModelFactory modelFactory = context.getModelFactory();

        model.remove(0);
        model.insert(0, modelFactory.createOpenElementTag("li"));
        model.insert(1, modelFactory.createOpenElementTag("span"));
        // position 2 is the text of the entry element
        model.insert(3, modelFactory.createCloseElementTag("span"));

        model.remove(model.size() - 1);
        model.insert(model.size() - 1, modelFactory.createCloseElementTag("li"));
    }
}
