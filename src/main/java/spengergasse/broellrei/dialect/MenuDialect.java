package spengergasse.broellrei.dialect;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

import java.util.HashSet;
import java.util.Set;

public class MenuDialect extends AbstractProcessorDialect {

    public MenuDialect() {
        super("Menu Dialect", "menu", 1000);
    }

    public Set<IProcessor> getProcessors(final String dialectPrefix) {
        final Set<IProcessor> processors = new HashSet<IProcessor>();
        processors.add(new DropdownMenuElementModelProcessor(dialectPrefix));
        processors.add(new DropdownEntryElementModelProcessor(dialectPrefix));
        processors.add(new DropdownSubMenuElementModelProcessor(dialectPrefix));
        return processors;
    }
}
