package spengergasse.broellrei.dialect;

import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.processor.element.AbstractElementModelProcessor;
import org.thymeleaf.processor.element.IElementModelStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;

public class DropdownSubMenuElementModelProcessor extends AbstractElementModelProcessor {

    public DropdownSubMenuElementModelProcessor(final String dialectPrefix) {

        super(
                TemplateMode.HTML,  // This processor will apply only to HTML mode
                dialectPrefix,      // Prefix to be applied to name for matching
                "dropsub",          // No tag name: match any tag name
                false,              // No prefix to be applied to tag name
                null,               // Name of the attribute that will be matched
                false,              // Apply dialect prefix to attribute name
                1000000);           // Precedence (inside dialect's precedence)
    }

    protected void doProcess(ITemplateContext context,
                             IModel model,
                             IElementModelStructureHandler structureHandler) {

        IModelFactory modelFactory = context.getModelFactory();

        model.remove(0);
        model.insert(0, modelFactory.createOpenElementTag("ul", "class", "drop-sub"));

        model.remove(model.size() - 1);
        model.insert(model.size(), modelFactory.createCloseElementTag("ul"));
    }
}
