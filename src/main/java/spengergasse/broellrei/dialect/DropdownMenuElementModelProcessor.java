package spengergasse.broellrei.dialect;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.model.ITemplateEvent;
import org.thymeleaf.processor.element.*;
import org.thymeleaf.templatemode.TemplateMode;

import java.awt.event.ItemEvent;

public class DropdownMenuElementModelProcessor extends AbstractElementModelProcessor {

    public DropdownMenuElementModelProcessor(final String dialectPrefix) {

        super(
                TemplateMode.HTML,  // This processor will apply only to HTML mode
                dialectPrefix,      // Prefix to be applied to name for matching
                "dropdown",         // No tag name: match any tag name
                false,              // No prefix to be applied to tag name
                null,               // Name of the attribute that will be matched
                false,              // Apply dialect prefix to attribute name
                1000000);           // Precedence (inside dialect's precedence)
    }

    protected void doProcess(ITemplateContext context,
                             IModel model,
                             IElementModelStructureHandler structureHandler) {

        IModelFactory modelFactory = context.getModelFactory();

        model.remove(0);
        model.insert(0, modelFactory.createOpenElementTag("ul", "class", "drop-top"));
        model.insert(0, modelFactory.createOpenElementTag("nav", "class", "dropdown"));

        model.remove(model.size() - 1);
        model.insert(model.size() - 1, modelFactory.createCloseElementTag("ul"));
        model.insert(model.size(), modelFactory.createCloseElementTag("nav"));
    }
}